This file is part of easywtr_frontend
Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
SPDX-License-Identifier: AGPL-3.0-only


under construction

0. License
easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
easywtr_frontend is part of the software easyWTR

Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
SPDX-License-Identifier: AGPL-3.0-only

easyWTR is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3 of the License.

easyWTR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.

1. easyWTR description
2. easyWTR Frontend description
3. How to install on own infrastructure
3.1 Firebase
3.2 Gitlab
3.3 VSCode
    -> adapt /.vscode/template-launch.json
