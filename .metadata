# This file is part of easywtr_frontend
# easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
# easywtr_frontend is part of the software easyWTR
# 
# Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
# SPDX-License-Identifier: AGPL-3.0-only
#
# easyWTR is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3 of the License.
#
# easyWTR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.

# This file tracks properties of this Flutter project.
# Used by Flutter tool to assess capabilities and perform upgrades etc.
#
# This file should be version controlled.

version:
  revision: 9944297138845a94256f1cf37beb88ff9a8e811a
  channel: stable

project_type: app

# Tracks metadata for the flutter migrate command
migration:
  platforms:
    - platform: root
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: android
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: ios
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: linux
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: macos
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: web
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
    - platform: windows
      create_revision: 9944297138845a94256f1cf37beb88ff9a8e811a
      base_revision: 9944297138845a94256f1cf37beb88ff9a8e811a

  # User provided section

  # List of Local paths (relative to this file) that should be
  # ignored by the migrate tool.
  #
  # Files that are not part of the templates will be ignored by default.
  unmanaged_files:
    - 'lib/main.dart'
    - 'ios/Runner.xcodeproj/project.pbxproj'
