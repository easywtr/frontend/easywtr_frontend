/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

import 'package:easyWTR/firebase_options.dart';
import 'package:easyWTR/screens/about/license.dart';
import 'package:easyWTR/screens/authenticate/register.dart';
import 'package:easyWTR/screens/authenticate/sign_in.dart';
import 'package:easyWTR/screens/home/home.dart';
import 'package:easyWTR/screens/home/profile.dart';
import 'package:easyWTR/screens/home/record_time.dart';
import 'package:easyWTR/screens/wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const Wrapper(),
      routes: <String, WidgetBuilder>{
        '/register': (BuildContext context) => const Register(),
        '/sign-in': (BuildContext context) => const MySignIn(),
        '/about': (BuildContext context) => const TermsOfUse(),
        '/record': (BuildContext context) => const RecordTime(),
        '/profile': (BuildContext context) => const MyProfile(),
        '/home': (BuildContext context) => const Home(),
      },
    );
  }
}
