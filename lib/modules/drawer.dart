/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

import 'package:easyWTR/modules/modules.dart';
import 'package:easyWTR/services/auth.dart';
import 'package:firebase_auth/firebase_auth.dart' hide EmailAuthProvider;
import 'package:flutter/material.dart';

/// The Drawer Menu of the Website is defined centrally and switches it's behavior
/// depending on the user's authentication status
class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.idTokenChanges(),
      builder: (context, snapshot) {
        //final user = snapshot.data;
        return Drawer(
          width: isWebMobile ? double.infinity : 400,
          child: ListView(
            children: [
              if (snapshot.hasData) ...[
                ListTile(
                  leading: const Icon(
                    Icons.circle,
                    size: 15,
                  ),
                  title: const Text('Zeiterfassung'),
                  onTap: () => Navigator.pushNamed(context, '/record'),
                  trailing: const Icon(Icons.access_time),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.circle,
                    size: 15,
                  ),
                  title: const Text('Profile'),
                  onTap: () => Navigator.pushNamed(context, '/profile'),
                  trailing: const Icon(Icons.person),
                ),
                ListTile(
                  title: const Text('Settings'),
                  onTap: () {
                    // Navigate to settings page
                  },
                ),
                ListTile(
                  leading: const Icon(
                    Icons.expand_more,
                    size: 20,
                  ),
                  title: const Text('Über diese Seite'),
                  onTap: () => Navigator.pushNamed(context, '/about'),
                  trailing: const Icon(Icons.question_mark),
                ),
                ListTile(
                  title: const Text('Abmelden'),
                  onTap: () async {
                    // Sign out the user
                    await _auth.signOut(context);
                  },
                ),
              ] else ...[
                ListTile(
                  leading: const Icon(
                    Icons.expand_more,
                    size: 20,
                  ),
                  title: const Text('Über diese Seite'),
                  onTap: () => Navigator.pushNamed(context, '/about'),
                  trailing: const Icon(Icons.question_mark),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.expand_more,
                    size: 20,
                  ),
                  title: const Text('Registrieren'),
                  onTap: () => Navigator.pushNamed(context, '/reg'),
                  trailing: const Icon(Icons.question_mark),
                ),
              ]
            ],
          ),
        );
      },
    );
  }
}
