/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

import 'package:easyWTR/services/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

/// The AppBar of the Website is defined centrally and switches it's behavior
/// depending on the user's authentication status
class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  const MyAppBar({super.key});

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  State<MyAppBar> createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  final AuthService _auth = AuthService();

  Future<void> _signOut(context) async {
    await _auth.signOut(context);
  }

  _signUpOrOutButtonFunction(bool userIsSignedIn, bool userIsOnSigninPage) {
    if (userIsSignedIn) {
      return _signOut(context);
    } else if (userIsOnSigninPage) {
      return Navigator.pushNamed(context, '/register');
    }
    return Navigator.pushNamed(context, '/sign-in');
  }

  Widget _title() {
    return const Text('easyWTR');
  }

  Widget _signUpOrOutButton(
    context,
    bool userIsSignedIn,
    String? currentroute,
  ) {
    final userIsOnSigninPage =
        currentroute == '/' || currentroute == '/sign-in';
    final buttonText = Text(userIsSignedIn
        ? 'Abmelden'
        : userIsOnSigninPage
            ? 'Registrieren'
            : 'Anmelden');
    final buttonIcon = Icon(userIsSignedIn
        ? Icons.logout
        : userIsOnSigninPage
            ? Icons.person
            : Icons.login);
    return TextButton.icon(
      onPressed: () =>
          _signUpOrOutButtonFunction(userIsSignedIn, userIsOnSigninPage),
      icon: buttonIcon,
      label: buttonText,
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.idTokenChanges(),
      builder: (context, snapshot) {
        bool userIsSignedIn = snapshot.hasData;
        var currentroute = ModalRoute.of(context)?.settings.name;
        return AppBar(
          backgroundColor: Colors.green[400],
          elevation: 0.0,
          title: _title(),
          actions: <Widget>[
            /// Top right AppBar button has different functions depending on the
            /// authentication status of the user and if the user is on the sign in or
            /// the sign up page when not signed in
            if (userIsSignedIn) ...[
              _signUpOrOutButton(context, userIsSignedIn, currentroute),
            ] else ...[
              _signUpOrOutButton(context, userIsSignedIn, currentroute),
            ]
          ],
        );
      },
    );
  }
}
