import 'package:easyWTR/modules/appbar.dart';
import 'package:easyWTR/modules/drawer.dart';
import 'package:firebase_ui_auth/firebase_ui_auth.dart';
import 'package:flutter/material.dart';

class MyProfile extends StatefulWidget {
  final dynamic toggleView;
  const MyProfile({super.key, this.toggleView});

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: const MyAppBar(),
      drawer: const MyDrawer(),
      body: ProfileScreen(
        providers: [
          EmailAuthProvider(),
        ],
        avatarSize: 24,
      ),
    );
  }
}
