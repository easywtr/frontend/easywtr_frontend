/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

import 'package:easyWTR/modules/appbar.dart';
import 'package:easyWTR/modules/drawer.dart';
import 'package:easyWTR/modules/modules.dart';
import 'package:easyWTR/services/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RecordTime extends StatefulWidget {
  const RecordTime({super.key});

  @override
  State<RecordTime> createState() => _RecordTimeState();
}

class _RecordTimeState extends State<RecordTime> {
  bool isWorking = false;
  var _startTime = DateTime.now();
  var _endTime = DateTime.now();
  Duration _workTime = const Duration(
    hours: 0,
    minutes: 0,
    seconds: 0,
  );
  final AuthService _auth = AuthService();

  _startWork() {
    if (_endTime.day != DateTime.now().day &&
        _workTime != const Duration(microseconds: 0)) {
      _workTime = const Duration(
        hours: 0,
        minutes: 0,
        seconds: 0,
      );
    }
    _startTime = DateTime.now();
    return _startTime;
  }

  _stopWork() {
    _endTime = DateTime.now();
    if (_endTime.day == DateTime.now().day) {
      _workTime = _workTime + _calcWorkTime(_startTime, _endTime);
    } else {
      _workTime = _calcWorkTime(_startTime, _endTime);
    }
    return _workTime;
  }

  _calcWorkTime(DateTime startTime, DateTime endTime) {
    _workTime = _endTime.difference(_startTime);
    return _workTime;
  }

  Widget _startStopButton() {
    return SizedBox(
      height: 200,
      width: 200,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100.0),
          ),
          backgroundColor: isWorking ? Colors.red : Colors.green,
        ),
        onPressed: () {
          if (!isWorking) {
            _startWork();
          } else {
            _stopWork();
          }
          setState(() => isWorking = !isWorking);
        },
        child: Text(
          isWorking ? 'Arbeit beenden' : 'Arbeit beginnen',
          style: const TextStyle(
            fontSize: 30,
          ),
        ),
      ),
    );
  }

  Widget _dynamicText() {
    if (_workTime == const Duration(microseconds: 0) && !isWorking) {
      return const Text('Neuer Tag, neues Glück');
    } else if (isWorking) {
      return Text('Arbeitszeitbeginn $_startTime');
    } else {
      return Text('Arbeitszeit heute $_workTime');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: const MyAppBar(),
      drawer: const MyDrawer(),
      body: Align(
        alignment: Alignment.center,
        child: Container(
          height: isWebMobile ? double.infinity : 500,
          width: isWebMobile ? double.infinity : 500,
          padding: const EdgeInsets.all(20),
          decoration: !isWebMobile
              ? BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                )
              : null,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _startStopButton(),
              _dynamicText(),
            ],
          ),
        ),
      ),
    );
  }
}
