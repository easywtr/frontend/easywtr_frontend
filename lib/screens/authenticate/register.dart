import 'package:easyWTR/modules/appbar.dart';
import 'package:easyWTR/modules/drawer.dart';
import 'package:firebase_ui_auth/firebase_ui_auth.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({super.key});

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final provider = [
    EmailAuthProvider(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: const MyAppBar(),
      drawer: const MyDrawer(),
      body: RegisterScreen(
        providers: provider,
        actions: [
          AuthStateChangeAction((context, state) {
            Navigator.pushReplacementNamed(context, '/rec');
          }),
        ],
      ),
    );
  }
}
