/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

import 'package:easyWTR/modules/drawer.dart';
import 'package:easyWTR/modules/modules.dart';
import 'package:easyWTR/services/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignIn extends StatefulWidget {
  final dynamic toggleView;
  const SignIn({super.key, this.toggleView});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String? errorMessage = '';

  bool _pwVisible = false;

  final TextEditingController _controllerEmail = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();

  final AuthService _auth = AuthService();

  Future<void> _displayErrorDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Fehler'),
            content: Text(errorMessage.toString()),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('OK'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  Future<void> _verifyEmailDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Fehler'),
            content: const Text(
                'Email Adresse ist nicht verifiziert. Email erneut senden?'),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('Abbruch'),
                onPressed: () {
                  setState(() {
                    _auth.signOut();
                    Navigator.pop(context);
                  });
                },
              ),
              ElevatedButton(
                child: const Text('OK'),
                onPressed: () {
                  setState(() {
                    _auth.verifyEmailAddress();
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  Future<void> signInWithEmailAndPassword() async {
    try {
      await _auth
          .signInWithEmailAndPassword(
            email: _controllerEmail.text,
            password: _controllerPassword.text,
          )
          .then((currentUser) => {
                if (!_auth.currentUser!.emailVerified)
                  {_verifyEmailDialog(context)}
              });
    } on FirebaseAuthException catch (e) {
      setState(() {
        errorMessage = e.message;
        _displayErrorDialog(context);
      });
    }
  }

  Widget _title() {
    return const Text('easyWTR Login');
  }

  Widget _entryFieldEmail(
    String title,
    TextEditingController controller,
  ) {
    return AutofillGroup(
      child: TextField(
        controller: controller,
        autofillHints: const <String>[AutofillHints.username],
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: title,
        ),
      ),
    );
  }

  Widget _entryFieldPassword(
    String title,
    TextEditingController controller,
  ) {
    return AutofillGroup(
      child: TextField(
        controller: controller,
        autofillHints: const <String>[AutofillHints.password],
        obscureText: !_pwVisible,
        decoration: InputDecoration(
          labelText: title,
          suffixIcon: IconButton(
            icon: Icon(
              _pwVisible ? Icons.visibility : Icons.visibility_off,
              color: Theme.of(context).primaryColorDark,
            ),
            onPressed: () {
              setState(() {
                _pwVisible = !_pwVisible;
              });
            },
          ),
        ),
        onEditingComplete: () => TextInput.finishAutofillContext(),
      ),
    );
  }

  Widget _submitButton() {
    return ElevatedButton(
      onPressed: () {
        signInWithEmailAndPassword();
      },
      child: const Text('Anmelden'),
    );
  }

  Widget _verticalSpacer(double space) {
    return SizedBox(height: space);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: AppBar(
        backgroundColor: Colors.green[400],
        elevation: 0.0,
        title: _title(),
        actions: <Widget>[
          TextButton.icon(
            onPressed: () {
              widget.toggleView();
            },
            icon: const Icon(Icons.person),
            label: const Text('Registrieren'),
          ),
        ],
      ),
      drawer: const MyDrawer(),
      body: Align(
        alignment: Alignment.center,
        child: Container(
          height: isWebMobile ? double.infinity : 500,
          width: isWebMobile ? double.infinity : 500,
          padding: const EdgeInsets.all(20),
          decoration: !isWebMobile
              ? BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                )
              : null,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _entryFieldEmail('Email', _controllerEmail),
              _verticalSpacer(20),
              _entryFieldPassword('Passwort', _controllerPassword),
              _verticalSpacer(50),
              _submitButton(),
            ],
          ),
        ),
      ),
    );
  }
}
