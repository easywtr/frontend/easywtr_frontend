/*This file is part of easywtr_frontend
  easywtr_frontend - Flutter Frontend for the free work time tracking web app easyWTR
  easywtr_frontend is part of the software easyWTR

  Copyright (C) 2023  Carsten Ritter (contact: easywtr@gmail.com)
  SPDX-License-Identifier: AGPL-3.0-only

  easyWTR is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, version 3 of the License.

  easyWTR is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with easyWTR.  If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.*/

#ifndef FLUTTER_MY_APPLICATION_H_
#define FLUTTER_MY_APPLICATION_H_

#include <gtk/gtk.h>

G_DECLARE_FINAL_TYPE(MyApplication, my_application, MY, APPLICATION,
                     GtkApplication)

/**
 * my_application_new:
 *
 * Creates a new Flutter-based application.
 *
 * Returns: a new #MyApplication.
 */
MyApplication* my_application_new();

#endif  // FLUTTER_MY_APPLICATION_H_
